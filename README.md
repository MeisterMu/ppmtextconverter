## Readme

My health is currently unstable, it is uncertain how up to date this will be.

Current issues
* Only works for 60fps recordings, the default. (if not, it is too slow or too fast - the output frames. youll still get something)
* a 4 minute clip will leave you unity unresponsive for ~5 or more minutes. It will use large amounts of cpu, beware.
* world position is currently semi bugged, remove from clip if bug.


---

1. Get ConvertPpmToAnim.cs or clone repo into a Unity2018 project.
2. Window>PPMtool
3. Specify paths.  

	![](https://i.imgur.com/1WC7Xq8.png)
  

4. Press generate.

---
.anim notes

* The clip generated is transform data associated with ppm_armature.fbx, it is the not a humanoid clip.  
  So in order to change into a humanoid clip, you'll have to attach the animation to that armature.  
  
	The easiest way I've found is to export the model with animation then reimport - possible with animation add-ons? I'm not a unity developer.
  I personally used fbxexporter.

* During recording, it doesn't matter which avatar you used to record with. Only this armature is needed.

* You can trim/keyframe reduce animations through add-ons or unity default reducer(?), this tool will not do that for you.  


---




faq

why does the code suck  
![](https://i.imgur.com/k0rmV4b.jpg) im tired 

