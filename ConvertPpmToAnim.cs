﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System.IO;
using System;
using System.Collections;
using System.Globalization;

public class ConvertPpmToAnim : EditorWindow
{
    #region it's hideous stay away
    #region Variables

    #region from udon world
    private const int BUFFER_SIZE = 14400;

    #region PARSE
    private bool framesProcessBool = false; //intermediate bool
    private bool frameProcessTerm = false;
    private bool framesParseBool = false;
    private bool frameParseTerm = false;
    private bool term = false;
    private string _parseMessage = "";

    //private string textFromFile = "";
    private float fps;

    private int[] leftBracketMap = new int[(54 * BUFFER_SIZE) + 1];// + 1 for start/end
    private int iLBM = 1;

    // 53 + 1 = 54 per
    private int[] rightBracketMap = new int[(54 * BUFFER_SIZE) + 1]; // + 1 for start/end

    private int iRBM = 0;

    // 2 + 1 * 2, 3 + 1 * 50, 3, + 1 = 209 per
    private int[] commaMap = new int[(210 * BUFFER_SIZE) - 1];

    private int iCM = 0;

    private string unparsedData = "";
    private int startProcessi = 0;
    private char[] unparsedDataCharArray;

    private int maxParseLength = 0;
    private int startParsei = 0;
    private const int INCREMENTPARSE = 1000;
    private const int INCREMENTPROCESS = 3;
    #endregion PARSE

    #region quarts
    private Vector3[] rpos = new Vector3[BUFFER_SIZE];
    private Vector3[] phips = new Vector3[BUFFER_SIZE];
    private Quaternion[] rhips;
    private Quaternion[] rspine;
    private Quaternion[] rchest;
    private Quaternion[] rneck;
    private Quaternion[] rhead;
    private Quaternion[] rleftUpperLeg;
    private Quaternion[] rrightUpperLeg;
    private Quaternion[] rleftLowerLeg;
    private Quaternion[] rrightLowerLeg;
    private Quaternion[] rleftFoot;
    private Quaternion[] rrightFoot;
    private Quaternion[] rleftToes;
    private Quaternion[] rrightToes;
    private Quaternion[] rleftShoulder;
    private Quaternion[] rrightShoulder;
    private Quaternion[] rleftUpperArm;
    private Quaternion[] rrightUpperArm;
    private Quaternion[] rleftLowerArm;
    private Quaternion[] rrightLowerArm;
    private Quaternion[] rleftHand;
    private Quaternion[] rrightHand;
    private Quaternion[] rleftThumbProximal;
    private Quaternion[] rleftThumbIntermediate;
    private Quaternion[] rleftThumbDistal;
    private Quaternion[] rleftIndexProximal;
    private Quaternion[] rleftIndexIntermediate;
    private Quaternion[] rleftIndexDistal;
    private Quaternion[] rleftMiddleProximal;
    private Quaternion[] rleftMiddleIntermediate;
    private Quaternion[] rleftMiddleDistal;
    private Quaternion[] rleftRingProximal;
    private Quaternion[] rleftRingIntermediate;
    private Quaternion[] rleftRingDistal;
    private Quaternion[] rleftLittleProximal;
    private Quaternion[] rleftLittleIntermediate;
    private Quaternion[] rleftLittleDistal;
    private Quaternion[] rrightThumbProximal;
    private Quaternion[] rrightThumbIntermediate;
    private Quaternion[] rrightThumbDistal;
    private Quaternion[] rrightIndexProximal;
    private Quaternion[] rrightIndexIntermediate;
    private Quaternion[] rrightIndexDistal;
    private Quaternion[] rrightMiddleProximal;
    private Quaternion[] rrightMiddleIntermediate;
    private Quaternion[] rrightMiddleDistal;
    private Quaternion[] rrightRingProximal;
    private Quaternion[] rrightRingIntermediate;
    private Quaternion[] rrightRingDistal;
    private Quaternion[] rrightLittleProximal;
    private Quaternion[] rrightLittleIntermediate;
    private Quaternion[] rrightLittleDistal;
    #endregion quarts
    #endregion from udon world

    // options
    public bool showLogGUI = false;

    private IEnumerator jobRoutine;
    private IEnumerator messageRoutine;

    private float progressCount = 0f;
    private float totalCount = 1f;

    #endregion Variables

    #region ui code

    #region methods

    private IEnumerator DisplayMessage(string message, float duration = 0f)
    {
        if (duration <= 0f || string.IsNullOrEmpty(message))
            goto Exit;

        _message = message;

        while (duration > 0)
        {
            duration -= 0.01667f;
            yield return null;
        }

    Exit:
        _message = string.Empty;
    }

    private IEnumerator ExportAnimationClip()
    {
        DisplayMessage(string.Empty);

        GenerateAnimationClip();
        //WriteFileLines(new List<string>() { ImagesRelPath, ImagesAbsPath, AnimationClipLocation, AnimationClipFileName });

        yield return null;
        jobRoutine = null;
    }

    private int endedTotalIndex = 0;

    private void GuiLine(int i_height = 1)

    {
        Rect rect = EditorGUILayout.GetControlRect(false, i_height);

        rect.height = i_height;

        EditorGUI.DrawRect(rect, new Color(0.5f, 0.5f, 0.5f, 1));
    }

    private static string ReadFile(string name)
    {
        string line;
        using (StreamReader sr = new StreamReader(name))
        {
            line = sr.ReadToEnd();
        }
        return line;
    }

    private void ShowExplorer(string itemPath)
    {
        itemPath = itemPath.Replace(@"/", @"\");   // explorer doesn't like front slashes
        System.Diagnostics.Process.Start("explorer.exe", "/select," + itemPath);
    }

    #endregion methods

    private float NormalizedProgress
    {
        get { return progressCount / totalCount; }
    }

    private float Progress
    {
        get { return progressCount / totalCount * 100f; }
    }

    private string FormattedProgress
    {
        get { return Progress.ToString("0.00") + "%"; }
    }

    #region Script Lifecycle

    [MenuItem("Window/PPMtool")]
    private static void Init()
    {
        try
        {
            //ReadFileLines();
        }
        catch (Exception e) { Console.WriteLine(e); }

        var window = (ConvertPpmToAnim)EditorWindow.GetWindow(typeof(ConvertPpmToAnim));
        window.Show();
    }

    public void OnInspectorUpdate()
    {
        Repaint();
    }

    private string objNames = "";

    // save file path
    public static string ImagesAbsPath = "L:/";

    public static string TextFileName = "4mintest.txt";
    public static string AnimationClipLocation = "Assets/PPM/ppmtextconverter/converted/";
    public static string AnimationClipFileName = "PPM";

    public int max_loop = 10000000;
    private string exportFilePath = "";
    public bool regenerateMaterials = true; //re generateexisting materials
    private string _message = null;

    /// <summary>
    /// If you happen to have seen this UI code before, it's code that I've written under a different name
    /// </summary>
    private void OnGUI()
    {
        EditorGUILayout.LabelField("", EditorStyles.boldLabel);
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        GuiLine(2);

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        GuiLine(1);
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        ImagesAbsPath = EditorGUILayout.TextField(
        "Text AbsolutePath",
        ImagesAbsPath);
        EditorGUILayout.Space();
        TextFileName = EditorGUILayout.TextField(
            ImagesAbsPath,
            TextFileName);
        EditorGUILayout.Space();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Test ");
        if (GUILayout.Button("Test Absolute Path (Win. Only)"))
        {
            ShowExplorer(ImagesAbsPath);
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();

        EditorGUILayout.Space();
        GuiLine(1);

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        AnimationClipLocation = EditorGUILayout.TextField(
        "Animation Clip Save Path",
        AnimationClipLocation);
        AnimationClipFileName = EditorGUILayout.TextField(
            "Animation Clip Name",
            AnimationClipFileName);
        exportFilePath = AnimationClipLocation + AnimationClipFileName + ".anim"; ;

        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Export File Path", EditorStyles.boldLabel);
        EditorGUILayout.LabelField(exportFilePath);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.LabelField("Warning: High CPU usage - Unity may be unresponsive.");
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        jobRoutineButton("Generate .anim");
        EditorGUILayout.LabelField("-", new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleCenter }, GUILayout.ExpandWidth(true));

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
    }

    private void OnEnable()
    {
        EditorApplication.update += HandleCallbackFunction;
    }

    private void HandleCallbackFunction()
    {
        if (jobRoutine != null && !jobRoutine.MoveNext())
            jobRoutine = null;

        if (messageRoutine != null && !messageRoutine.MoveNext())
            messageRoutine = null;
    }

    private void OnDisable()
    {
        EditorApplication.update -= HandleCallbackFunction;
    }

    #endregion Script Lifecycle

    private void jobRoutineButton(String buttonname)
    {
        string buttonLabel = jobRoutine != null ? "Cancel current task" : buttonname;
        if (GUILayout.Button(buttonLabel))
        {
            if (jobRoutine != null)
            {
                messageRoutine = DisplayMessage("Cancelled. " + FormattedProgress + " complete!", 4f);
                jobRoutine = null;
            }
            else
            {
                jobRoutine = ExportAnimationClip();
            }
        }

        if (jobRoutine != null)
        {
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.PrefixLabel(FormattedProgress);

            var rect = EditorGUILayout.GetControlRect();
            rect.width = rect.width * NormalizedProgress;
            GUI.Box(rect, GUIContent.none);

            EditorGUILayout.EndHorizontal();
        }
        else if (!string.IsNullOrEmpty(_message))
        {
            EditorGUILayout.HelpBox(_message, MessageType.None);
        }
    }

    #endregion ui code

    #region from udon world

    private void resetQuarts()
    {
        #region init

        rpos = new Vector3[BUFFER_SIZE];
        phips = new Vector3[BUFFER_SIZE];
        rhips = new Quaternion[BUFFER_SIZE];
        rspine = new Quaternion[BUFFER_SIZE];
        rchest = new Quaternion[BUFFER_SIZE];
        rneck = new Quaternion[BUFFER_SIZE];
        rhead = new Quaternion[BUFFER_SIZE];
        rleftUpperLeg = new Quaternion[BUFFER_SIZE];
        rrightUpperLeg = new Quaternion[BUFFER_SIZE];
        rleftLowerLeg = new Quaternion[BUFFER_SIZE];
        rrightLowerLeg = new Quaternion[BUFFER_SIZE];
        rleftFoot = new Quaternion[BUFFER_SIZE];
        rrightFoot = new Quaternion[BUFFER_SIZE];
        rleftToes = new Quaternion[BUFFER_SIZE];
        rrightToes = new Quaternion[BUFFER_SIZE];
        rleftShoulder = new Quaternion[BUFFER_SIZE];
        rrightShoulder = new Quaternion[BUFFER_SIZE];
        rleftUpperArm = new Quaternion[BUFFER_SIZE];
        rrightUpperArm = new Quaternion[BUFFER_SIZE];
        rleftLowerArm = new Quaternion[BUFFER_SIZE];
        rrightLowerArm = new Quaternion[BUFFER_SIZE];
        rleftHand = new Quaternion[BUFFER_SIZE];
        rrightHand = new Quaternion[BUFFER_SIZE];
        rleftThumbProximal = new Quaternion[BUFFER_SIZE];
        rleftThumbIntermediate = new Quaternion[BUFFER_SIZE];
        rleftThumbDistal = new Quaternion[BUFFER_SIZE];
        rleftIndexProximal = new Quaternion[BUFFER_SIZE];
        rleftIndexIntermediate = new Quaternion[BUFFER_SIZE];
        rleftIndexDistal = new Quaternion[BUFFER_SIZE];
        rleftMiddleProximal = new Quaternion[BUFFER_SIZE];
        rleftMiddleIntermediate = new Quaternion[BUFFER_SIZE];
        rleftMiddleDistal = new Quaternion[BUFFER_SIZE];
        rleftRingProximal = new Quaternion[BUFFER_SIZE];
        rleftRingIntermediate = new Quaternion[BUFFER_SIZE];
        rleftRingDistal = new Quaternion[BUFFER_SIZE];
        rleftLittleProximal = new Quaternion[BUFFER_SIZE];
        rleftLittleIntermediate = new Quaternion[BUFFER_SIZE];
        rleftLittleDistal = new Quaternion[BUFFER_SIZE];
        rrightThumbProximal = new Quaternion[BUFFER_SIZE];
        rrightThumbIntermediate = new Quaternion[BUFFER_SIZE];
        rrightThumbDistal = new Quaternion[BUFFER_SIZE];
        rrightIndexProximal = new Quaternion[BUFFER_SIZE];
        rrightIndexIntermediate = new Quaternion[BUFFER_SIZE];
        rrightIndexDistal = new Quaternion[BUFFER_SIZE];
        rrightMiddleProximal = new Quaternion[BUFFER_SIZE];
        rrightMiddleIntermediate = new Quaternion[BUFFER_SIZE];
        rrightMiddleDistal = new Quaternion[BUFFER_SIZE];
        rrightRingProximal = new Quaternion[BUFFER_SIZE];
        rrightRingIntermediate = new Quaternion[BUFFER_SIZE];
        rrightRingDistal = new Quaternion[BUFFER_SIZE];
        rrightLittleProximal = new Quaternion[BUFFER_SIZE];
        rrightLittleIntermediate = new Quaternion[BUFFER_SIZE];
        rrightLittleDistal = new Quaternion[BUFFER_SIZE];

        #endregion init
    }

    private NumberFormatInfo culture = CultureInfo.InvariantCulture.NumberFormat;

    private void parse(string textFromFile)
    {
        resetQuarts();
        //Get fps
        //Initialize states
        //State for if in frame or not
        //State for which index -- hip position to toes quarts
        //State for which axis -- x y z w
        bool inFrames = false;
        int inFrameIdx = 0;
        bool readFPS = false;
        int hreadIdx = 0;
        int cTempSize = 15;
        var cTemp = new char[cTempSize];
        var ci = 0;
        // The loop
        int i = 0;
        for (; ; )
        {
            if (i >= textFromFile.Length)
            {
                Debug.Log("Reached end of file.");
                break;
            }

            var currC = textFromFile[i];
            if (!readFPS)
            {
                if (currC == ',')
                {
                    //cTemp[ci] = '\0';
                    fps = float.Parse(new string(cTemp), culture);

                    readFPS = true;
                    ci = 0;
                    cTemp = new char[cTempSize];
                }
                cTemp[ci] = currC;
                ci++;
            }
            else if (inFrames)
            {
                //if (currC == ']' && ci - 1 > 0 && cTemp[ci - 1] == ']')
                if (hreadIdx > 52) // or currC == ']'
                {
                    inFrameIdx++;
                    hreadIdx = 0;
                    cTemp = new char[20];
                    if (currC != '[') // no next frame
                    {
                        Debug.Log("End of frames (2)");
                        break;
                    }
                    i++; //]],[[
                }
                else
                {
                    switch (hreadIdx)
                    {
                        case 0:
                            rpos[inFrameIdx] = readV3(ref i, textFromFile);// When in U#, strings will not pass be passed by reference **
                            break;

                        case 1:
                            phips[inFrameIdx] = readV3(ref i, textFromFile);
                            break;

                        case 2:
                            rhips[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 3:
                            rspine[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 4:
                            rchest[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 5:
                            rneck[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 6:
                            rhead[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 7:
                            rleftUpperLeg[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 8:
                            rrightUpperLeg[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 9:
                            rleftLowerLeg[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 10:
                            rrightLowerLeg[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 11:
                            rleftFoot[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 12:
                            rrightFoot[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 13:
                            rleftToes[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 14:
                            rrightToes[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 15:
                            rleftShoulder[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 16:
                            rrightShoulder[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 17:
                            rleftUpperArm[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 18:
                            rrightUpperArm[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 19:
                            rleftLowerArm[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 20:
                            rrightLowerArm[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 21:
                            rleftHand[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 22:
                            rrightHand[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 23:
                            rleftThumbProximal[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 24:
                            rleftThumbIntermediate[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 25:
                            rleftThumbDistal[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 26:
                            rleftIndexProximal[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 27:
                            rleftIndexIntermediate[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 28:
                            rleftIndexDistal[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 29:
                            rleftMiddleProximal[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 30:
                            rleftMiddleIntermediate[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 31:
                            rleftMiddleDistal[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 32:
                            rleftRingProximal[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 33:
                            rleftRingIntermediate[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 34:
                            rleftRingDistal[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 35:
                            rleftLittleProximal[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 36:
                            rleftLittleIntermediate[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 37:
                            rleftLittleDistal[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 38:
                            rrightThumbProximal[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 39:
                            rrightThumbIntermediate[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 40:
                            rrightThumbDistal[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 41:
                            rrightIndexProximal[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 42:
                            rrightIndexIntermediate[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 43:
                            rrightIndexDistal[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 44:
                            rrightMiddleProximal[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 45:
                            rrightMiddleIntermediate[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 46:
                            rrightMiddleDistal[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 47:
                            rrightRingProximal[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 48:
                            rrightRingIntermediate[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 49:
                            rrightRingDistal[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 50:
                            rrightLittleProximal[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 51:
                            rrightLittleIntermediate[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;

                        case 52:
                            rrightLittleDistal[inFrameIdx] = readQuat(ref i, textFromFile);
                            break;
                    }
                    i++; // 0.001],[...
                    hreadIdx++;
                }
            }
            else if (!inFrames)
            {
                if (currC == '[' && ci - 1 > 0 && cTemp[ci - 1] == '[')
                {
                    inFrames = true;
                    hreadIdx = 0;
                }
                else if (currC != '[' && currC != ',')
                {
                    Debug.Log("End of frames");
                    break;
                }

                cTemp[ci] = currC;
                ci++;
            }

            i++;
        }

        endedTotalIndex = inFrameIdx;
    }

    private Vector3 readV3(ref int i, string textFromFile)
    {
        float floatx = readFloatString(ref i, textFromFile, ',');
        i++;
        float floaty = readFloatString(ref i, textFromFile, ',');
        i++;
        float floatz = readFloatString(ref i, textFromFile, ']');
        i++;
        return new Vector3(floatx, floaty, floatz);
    }

    private Quaternion readQuat(ref int i, string textFromFile)
    {
        float floatx = readFloatString(ref i, textFromFile, ',');
        i++;
        float floaty = readFloatString(ref i, textFromFile, ',');
        i++;
        float floatz = readFloatString(ref i, textFromFile, ',');
        i++;
        float floatw = readFloatString(ref i, textFromFile, ']');
        i++;
        return new Quaternion(floatx, floaty, floatz, floatw);
    }

    private float readFloatString(ref int i, string textFromFile, char endChar)
    {
        var ii = 0;
        char tchar = textFromFile[i];
        var tv = new char[20];

        while (tchar != endChar)
        {
            tv[ii] = tchar;
            ii++;
            i++;
            tchar = textFromFile[i];
        }

        /*
        try
        {
            var f = float.Parse(new string(tv), culture);
            return f;
        }
        catch
        {
            return 0;
        }
        */

        var periodCount = 0;
        foreach (var c in tv)
        {
            if (c == '.')
                periodCount++;
            else if (c == '\0')
                break;
        }
        if (periodCount == 2)
            tv[0] = '-';

        var newString = new string(tv);
        return float.Parse(newString, culture);
    }

    #endregion from udon world

    #region generate the animation clip

    private void GenerateAnimationClip()
    {
        AnimationClip clip = new AnimationClip();
        clip.name = AnimationClipFileName;

        #region Init and check
        try
        {
            if (Directory.Exists(ImagesAbsPath))
            {
            }
            else
            {
                DisplayMessage("The path location does not exist: " + ImagesAbsPath, 5f);
            }
        }
        catch (IOException ex)
        {
            Console.WriteLine(ex.Message);
            return;
        }

        var rawtext = ReadFile(ImagesAbsPath + TextFileName);

        var decoded0 = convertBack1(rawtext);

        var decoded = convertBack(decoded0);

        parse(decoded);
        float framespersecond = fps; // set in parse

        progressCount = 0f;
        #endregion Init and check

        #region
        var a = new SignAnimationCreator[]
        {
             new SignAnimationCreator("Armature/Hips", null, "Hips"),
             new SignAnimationCreator("Armature/Hips/Spine", null, "Spine"),
             new SignAnimationCreator("Armature/Hips/Spine/Chest", null, "Chest"),
             new SignAnimationCreator("Armature/Hips/Spine/Chest/Neck", null, "Neck"),
             new SignAnimationCreator("Armature/Hips/Spine/Chest/Neck/Head", null, "Head"),
             new SignAnimationCreator("Armature/Hips/Left leg", null, "Left leg"),
             new SignAnimationCreator("Armature/Hips/Right leg", null, "Right leg"),
             new SignAnimationCreator("Armature/Hips/Left leg/Left knee", null, "Left knee"),
             new SignAnimationCreator("Armature/Hips/Right leg/Right knee", null, "Right knee"),
             new SignAnimationCreator("Armature/Hips/Left leg/Left knee/Left ankle", null, "Left ankle"),//10
             new SignAnimationCreator("Armature/Hips/Right leg/Right knee/Right ankle", null, "Right ankle"),
             new SignAnimationCreator("Armature/Hips/Spine/Chest/Left shoulder", null, "Left shoulder"),
             new SignAnimationCreator("Armature/Hips/Spine/Chest/Right shoulder", null, "Right shoulder"),
             new SignAnimationCreator("Armature/Hips/Spine/Chest/Left shoulder/Left arm", null, "Left arm"),
             new SignAnimationCreator("Armature/Hips/Spine/Chest/Right shoulder/Right arm", null, "Right arm"),//15
             new SignAnimationCreator("Armature/Hips/Spine/Chest/Left shoulder/Left arm/Left elbow", null, "Left elbow"),
             new SignAnimationCreator("Armature/Hips/Spine/Chest/Right shoulder/Right arm/Right elbow", null, "Right elbow"),
             new SignAnimationCreator("Armature/Hips/Spine/Chest/Left shoulder/Left arm/Left elbow/Left wrist", null, "Left wrist"),
             new SignAnimationCreator("Armature/Hips/Spine/Chest/Right shoulder/Right arm/Right elbow/Right wrist", null, "Right wrist"),
             new SignAnimationCreator("Armature/Hips/Left leg/Left knee/Left ankle/Left toe", null, "Left toe"),//20
             new SignAnimationCreator("Armature/Hips/Right leg/Right knee/Right ankle/Right toe", null, "Right toe"),
             new SignAnimationCreator("", null, ""),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Left shoulder/Left arm/Left elbow/Left wrist/Thumb0_L", null, "Thumb0_L"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Left shoulder/Left arm/Left elbow/Left wrist/Thumb0_L/Thumb1_L", null, "Thumb1_L"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Left shoulder/Left arm/Left elbow/Left wrist/Thumb0_L/Thumb1_L/Thumb2_L", null, "Thumb2_L"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Left shoulder/Left arm/Left elbow/Left wrist/IndexFinger1_L", null, "IndexFinger1_L"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Left shoulder/Left arm/Left elbow/Left wrist/IndexFinger1_L/IndexFinger2_L", null, "IndexFinger2_L"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Left shoulder/Left arm/Left elbow/Left wrist/IndexFinger1_L/IndexFinger2_L/IndexFinger3_L", null, "IndexFinger3_L"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Left shoulder/Left arm/Left elbow/Left wrist/MiddleFinger1_L", null, "MiddleFinger1_L"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Left shoulder/Left arm/Left elbow/Left wrist/MiddleFinger1_L/MiddleFinger2_L", null, "MiddleFinger2_L"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Left shoulder/Left arm/Left elbow/Left wrist/MiddleFinger1_L/MiddleFinger2_L/MiddleFinger3_L", null, "MiddleFinger3_L"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Left shoulder/Left arm/Left elbow/Left wrist/RingFinger1_L", null, "RingFinger1_L"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Left shoulder/Left arm/Left elbow/Left wrist/RingFinger1_L/RingFinger2_L", null, "RingFinger2_L"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Left shoulder/Left arm/Left elbow/Left wrist/RingFinger1_L/RingFinger2_L/RingFinger3_L", null, "RingFinger3_L"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Left shoulder/Left arm/Left elbow/Left wrist/LittleFinger1_L", null, "LittleFinger1_L"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Left shoulder/Left arm/Left elbow/Left wrist/LittleFinger1_L/LittleFinger2_L", null, "LittleFinger2_L"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Left shoulder/Left arm/Left elbow/Left wrist/LittleFinger1_L/LittleFinger2_L/LittleFinger3_L", null, "LittleFinger3_L"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Right shoulder/Right arm/Right elbow/Right wrist/Thumb0_R", null, "Thumb0_R"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Right shoulder/Right arm/Right elbow/Right wrist/Thumb0_R/Thumb1_R", null, "Thumb1_R"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Right shoulder/Right arm/Right elbow/Right wrist/Thumb0_R/Thumb1_R/Thumb2_R", null, "Thumb2_R"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Right shoulder/Right arm/Right elbow/Right wrist/IndexFinger1_R", null, "IndexFinger1_R"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Right shoulder/Right arm/Right elbow/Right wrist/IndexFinger1_R/IndexFinger2_R", null, "IndexFinger2_R"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Right shoulder/Right arm/Right elbow/Right wrist/IndexFinger1_R/IndexFinger2_R/IndexFinger3_R", null, "IndexFinger3_R"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Right shoulder/Right arm/Right elbow/Right wrist/MiddleFinger1_R", null, "MiddleFinger1_R"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Right shoulder/Right arm/Right elbow/Right wrist/MiddleFinger1_R/MiddleFinger2_R", null, "MiddleFinger2_R"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Right shoulder/Right arm/Right elbow/Right wrist/MiddleFinger1_R/MiddleFinger2_R/MiddleFinger3_R", null, "MiddleFinger3_R"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Right shoulder/Right arm/Right elbow/Right wrist/RingFinger1_R", null, "RingFinger1_R"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Right shoulder/Right arm/Right elbow/Right wrist/RingFinger1_R/RingFinger2_R", null, "RingFinger2_R"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Right shoulder/Right arm/Right elbow/Right wrist/RingFinger1_R/RingFinger2_R/RingFinger3_R", null, "RingFinger3_R"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Right shoulder/Right arm/Right elbow/Right wrist/LittleFinger1_R", null, "LittleFinger1_R"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Right shoulder/Right arm/Right elbow/Right wrist/LittleFinger1_R/LittleFinger2_R", null, "LittleFinger2_R"),
            new SignAnimationCreator("Armature/Hips/Spine/Chest/Right shoulder/Right arm/Right elbow/Right wrist/LittleFinger1_R/LittleFinger2_R/LittleFinger3_R", null, "LittleFinger3_R"),
        };
        #endregion generate the animation clip

        #region
        Vector3 pos = Vector3.zero;

        // TODO: average xy ..
        int middleindex = endedTotalIndex / 2;
        var middlepos = rpos[middleindex];

        for (int i = 0; i < endedTotalIndex; i++)
        {
            float animTime = ((float)i) / 45;
            progressCount += 1f;
            #region
            a[0].AddFrame(animTime, phips[i], rhips[i]);
            a[1].AddFrame(animTime, pos, rspine[i]);
            a[2].AddFrame(animTime, pos, rchest[i]);
            a[3].AddFrame(animTime, pos, rneck[i]);
            a[4].AddFrame(animTime, pos, rhead[i]);
            a[5].AddFrame(animTime, pos, rleftUpperLeg[i]);
            a[6].AddFrame(animTime, pos, rrightUpperLeg[i]);
            a[7].AddFrame(animTime, pos, rleftLowerLeg[i]);
            a[8].AddFrame(animTime, pos, rrightLowerLeg[i]);
            a[9].AddFrame(animTime, pos, rleftFoot[i]);
            a[10].AddFrame(animTime, pos, rrightFoot[i]);
            a[11].AddFrame(animTime, pos, rleftShoulder[i]);
            a[12].AddFrame(animTime, pos, rrightShoulder[i]);
            a[13].AddFrame(animTime, pos, rleftUpperArm[i]);
            a[14].AddFrame(animTime, pos, rrightUpperArm[i]);
            a[15].AddFrame(animTime, pos, rleftLowerArm[i]);
            a[16].AddFrame(animTime, pos, rrightLowerArm[i]);
            a[17].AddFrame(animTime, pos, rleftHand[i]);
            a[18].AddFrame(animTime, pos, rrightHand[i]);
            a[19].AddFrame(animTime, pos, rleftToes[i]);// i20
            a[20].AddFrame(animTime, pos, rrightToes[i]);
            a[21].AddFrame(animTime, new Vector3(rpos[i].x - middlepos.x, rpos[i].y, rpos[i].z - middlepos.z), Quaternion.identity);
            #endregion it's hideous stay away
            a[22].AddFrame(animTime, pos, rleftThumbProximal[i]);
            a[23].AddFrame(animTime, pos, rleftThumbIntermediate[i]);
            a[24].AddFrame(animTime, pos, rleftThumbDistal[i]);
            a[25].AddFrame(animTime, pos, rleftIndexProximal[i]);
            a[26].AddFrame(animTime, pos, rleftIndexIntermediate[i]);
            a[27].AddFrame(animTime, pos, rleftIndexDistal[i]);
            a[28].AddFrame(animTime, pos, rleftMiddleProximal[i]);
            a[29].AddFrame(animTime, pos, rleftMiddleIntermediate[i]);
            a[30].AddFrame(animTime, pos, rleftMiddleDistal[i]);
            a[31].AddFrame(animTime, pos, rleftRingProximal[i]);
            a[32].AddFrame(animTime, pos, rleftRingIntermediate[i]);
            a[33].AddFrame(animTime, pos, rleftRingDistal[i]);
            a[34].AddFrame(animTime, pos, rleftLittleProximal[i]);
            a[35].AddFrame(animTime, pos, rleftLittleIntermediate[i]);
            a[36].AddFrame(animTime, pos, rleftLittleDistal[i]);
            a[37].AddFrame(animTime, pos, rrightThumbProximal[i]);
            a[38].AddFrame(animTime, pos, rrightThumbIntermediate[i]);
            a[39].AddFrame(animTime, pos, rrightThumbDistal[i]);
            a[40].AddFrame(animTime, pos, rrightIndexProximal[i]);
            a[41].AddFrame(animTime, pos, rrightIndexIntermediate[i]);
            a[42].AddFrame(animTime, pos, rrightIndexDistal[i]);
            a[43].AddFrame(animTime, pos, rrightMiddleProximal[i]);
            a[44].AddFrame(animTime, pos, rrightMiddleIntermediate[i]);
            a[45].AddFrame(animTime, pos, rrightMiddleDistal[i]);
            a[46].AddFrame(animTime, pos, rrightRingProximal[i]);
            a[47].AddFrame(animTime, pos, rrightRingIntermediate[i]);
            a[48].AddFrame(animTime, pos, rrightRingDistal[i]);
            a[49].AddFrame(animTime, pos, rrightLittleProximal[i]);
            a[50].AddFrame(animTime, pos, rrightLittleIntermediate[i]);
            a[51].AddFrame(animTime, pos, rrightLittleDistal[i]);
        }

        AnimationCurveContainer[] curves = a[0].curves;
        for (int x = 0; x < curves.Length; x++)
        {
            clip.SetCurve("Armature/Hips", typeof(Transform), curves[x].propertyName, curves[x].animCurve);
        }

        for (int idx = 1; idx < 52; idx++)
        {
            var curvess = a[idx].curves;
            if (idx == 21)
            {
                for (int x = 0; x < curves.Length; x++)
                {
                    if (x >= 3)
                        continue;

                    clip.SetCurve(a[idx].pathName, typeof(Transform), curvess[x].propertyName, curvess[x].animCurve);
                }
            }
            else
            {
                for (int x = 0; x < curves.Length; x++)
                {
                    if (x < 3)
                        continue;

                    clip.SetCurve(a[idx].pathName, typeof(Transform), curvess[x].propertyName, curvess[x].animCurve);
                }
            }
        }

        #endregion

        clip.EnsureQuaternionContinuity();
        AssetDatabase.CreateAsset(clip, exportFilePath);
        AssetDatabase.SaveAssets();
        Debug.Log(".anim file generated to " + exportFilePath);
        messageRoutine = DisplayMessage("Animation Generation complete!", 6f);
    }

    private string convertBack1(string ss)
    {
        var sss = System.Text.Encoding.ASCII.GetBytes(ss);

        var letterMapInv = new int[9];
        letterMapInv[0] = 48; //0
        letterMapInv[1] = 49; //1
        letterMapInv[2] = 51; // 3
        letterMapInv[3] = 57;  // 9
        letterMapInv[4] = 124; //|
        letterMapInv[5] = 44; //,
        letterMapInv[6] = 46; //.
        letterMapInv[7] = 42; // *

        var css = new char[sss.Length * 2]; // 2 characters in 1 byte, + 1 for remainder
        Debug.Log("==================================");
        Debug.Log("char : " + (css.Length) + " : sss : " + sss.Length);
        int ci;
        int bl = 0;

        // When odd, there's an extra 0 at the end...

        for (ci = 0; ci < sss.Length; ci++)
        {
            css[bl] = (char)letterMapInv[(int)(sss[ci] & 0x07)];
            css[bl + 1] = (char)letterMapInv[(int)((sss[ci] >> 3) & 0x07)];
            bl += 2;
        }

        return new string(css);
    }

    private string convertBack(string s)
    {
        var comboMap2 = new char[100];
        comboMap2[48] = '8';
        comboMap2[49] = '7';
        comboMap2[44] = '6';
        comboMap2[51] = '5';
        comboMap2[57] = '4';
        comboMap2[46] = '2';

        var newSS = s.ToCharArray();
        var newSSS = new char[newSS.Length];
        var tss = 0;
        var prevChar = ' ';
        for (var newSSSi = 0; tss < newSS.Length; newSSSi++)
        {
            try
            {
                if (newSSSi >= newSSS.Length)
                    break;

                var ctemp = newSS[tss];
                switch (ctemp)
                {
                    case '*':
                        tss++;
                        if (tss >= newSS.Length)
                            break;
                        newSSS[newSSSi] = comboMap2[newSS[tss]];
                        if (newSSS[newSSSi] == 0) Debug.Log("Error!");
                        prevChar = ' ';
                        break;

                    case '|':
                        switch (prevChar)
                        {
                            case ',':
                                newSSS[newSSSi] = '[';
                                prevChar = '[';
                                break;

                            case '[':
                                newSSS[newSSSi] = '[';
                                prevChar = '[';
                                break;

                            case ']':
                                newSSS[newSSSi] = ']';
                                prevChar = ']';
                                break;

                            case ' ':
                                newSSS[newSSSi] = ']';
                                prevChar = ']';
                                break;
                        }
                        break;

                    case '.':
                        if (prevChar == '.')
                        {
                            newSSS[newSSSi - 1] = '-';
                            newSSS[newSSSi] = '.';
                        }
                        else
                        {
                            newSSS[newSSSi] = '.';
                        }
                        prevChar = '.';
                        break;

                    case ',':
                        if (prevChar == ',')
                        { // Odd at split
                            newSSSi--; // skip character
                        }
                        else
                        {
                            newSSS[newSSSi] = ',';
                        }
                        prevChar = ',';
                        break;

                    default:
                        newSSS[newSSSi] = ctemp;
                        prevChar = ' ';
                        break;
                }
                tss++;
            }
            catch
            {
                Debug.Log("");
            }
        }
        return new string(newSSS);
    }

    #endregion Logic
    #endregion
}

#region Pretty sure these are from some other open source unity mocap script, it's too readable

public class SignAnimationCreator
{
    public AnimationCurveContainer[] curves;
    public string pathName = "";

    public SignAnimationCreator(string hierarchyPath, Transform toolazytorefactorout = null, string andthis = null)
    {
        pathName = hierarchyPath;

        curves = new AnimationCurveContainer[7];

        curves[0] = new AnimationCurveContainer("localPosition.x");
        curves[1] = new AnimationCurveContainer("localPosition.y");
        curves[2] = new AnimationCurveContainer("localPosition.z");

        curves[3] = new AnimationCurveContainer("localRotation.x");
        curves[4] = new AnimationCurveContainer("localRotation.y");
        curves[5] = new AnimationCurveContainer("localRotation.z");
        curves[6] = new AnimationCurveContainer("localRotation.w");
    }

    public void AddFrame(float time, Vector3 pos, Quaternion rot)
    {
        curves[0].AddValue(time, pos.x);
        curves[1].AddValue(time, pos.y);
        curves[2].AddValue(time, pos.z);

        curves[3].AddValue(time, rot.x);
        curves[4].AddValue(time, rot.y);
        curves[5].AddValue(time, rot.z);
        curves[6].AddValue(time, rot.w);
    }
}

public class AnimationCurveContainer
{
    public string propertyName = "";
    public AnimationCurve animCurve;

    public AnimationCurveContainer(string _propertyName)
    {
        animCurve = new AnimationCurve();
        propertyName = _propertyName;
    }

    public void AddValue(float animTime, float animValue)
    {
        Keyframe key = new Keyframe(animTime, animValue, 0.0f, 0.0f);
        animCurve.AddKey(key);
    }
}

#endregion

#endif